/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <err.h>
#include <errno.h>
#include <sys/queue.h>
#include <arpa/inet.h>

#include "conf.h"
#include "utils.h"

/* types */
SIMPLEQ_HEAD(list, list_item);
struct list_item {
	void				*val;
	SIMPLEQ_ENTRY(list_item)	 next;
};

/* lex vars */
extern FILE		*yyin;
extern int	 	 yylineno;
extern int	 	 yylex(void);
/* global vars */
int		 	 yyerror(const char *, ...);
static ofp_fptr		 check_ofp_version(const char *);
static char		*filename;
static struct config	*conf;
%}

%union {
	int			 i;
	char			*s;
	struct list		*l;
}

%token		ID
%token		LISTEN ON
%token		PEERS CONTROLLER
%token		IP PORT
%token		REPLICAS
%token		OPENFLOW VERSION
%token		APPLICATION
%token		ADAPT EVERY SEC MIN
%token		LEARN USING WITH FOR EQUAL
%token		KMEANSSEQ CLUSTERS
%token		KMEANSINCR THRESHOLD
%token		LOG TO
%token		TOPOLOGY HOST AT DPID
%token		COMMA LCBRACKET RCBRACKET

%token<i>	BOOL
%token<i>	NUMBER
%token<s>	VAR
%token<s>	STRING

%type<l>	peer_list
%type<l>	str_list
%type<l>	param_list
%type<l>	host_list

%%

main		:
		| main id
		| main listen
		| main peers
		| main replicas
		| main openflow
		| main application
		| main adapt
		| main learn
		| main log
		| main topology
		;

id		: ID STRING {
			conf->c_nodeid = strdup($2);
		}
		;

listen		: LISTEN ON PORT NUMBER {
			conf->c_co_port = $4;
		}
		;

peer_list	: CONTROLLER STRING IP STRING PORT NUMBER {
			struct peer		*p = (struct peer *)malloc(sizeof(struct peer));
			struct list_item	*q = (struct list_item *)malloc(sizeof(struct list_item));

			p->node_id = strdup($2);
			if (!inet_aton($4, &p->ip_addr))
				errx(1, "invalid IP address");
			p->port = $6;

			q->val = p;
			$$ = (struct list *)malloc(sizeof(struct list));
			SIMPLEQ_INIT($$);
			SIMPLEQ_INSERT_TAIL($$, q, next);
		}
		| peer_list COMMA CONTROLLER STRING IP STRING PORT NUMBER {
			struct peer		*p = (struct peer *)malloc(sizeof(struct peer));
			struct list_item	*q = (struct list_item *)malloc(sizeof(struct list_item));

			p->node_id = strdup($4);
			if (!inet_aton($6, &p->ip_addr))
				errx(1, "invalid IP address");
			p->port = $8;

			q->val = p;
			SIMPLEQ_INSERT_TAIL($1, q, next);
		}
		;

peers		: PEERS LCBRACKET peer_list RCBRACKET {
			int			  i = 0, n = 0;
			struct list_item	 *p;

			SIMPLEQ_FOREACH(p, $3, next)
				++n;
			conf->c_npeers = n;
			conf->c_peers = (struct peer *)reallocarray(NULL, n, sizeof(struct peer));
			while (!SIMPLEQ_EMPTY($3)) {
				p = SIMPLEQ_FIRST($3);
				conf->c_peers[i++] = *((struct peer *)p->val);
				SIMPLEQ_REMOVE_HEAD($3, next);
				free(p);
			}
			free($3);
		}
		;

replicas	: REPLICAS NUMBER {
			conf->c_nreplicas = $2;
		}
		;

openflow	: OPENFLOW VERSION STRING {
			if ((conf->c_ofp = check_ofp_version($3)) == NULL) {
				yyerror("OpenFlow version '%s' not supported.", $3);
				YYABORT;
			}
			conf->c_ofp_verstr = strdup($3);
		}
		| OPENFLOW LISTEN ON PORT NUMBER {
			conf->c_sw_port = $5;
		}
		;

str_list	: STRING {
			struct list_item	*s = (struct list_item *)malloc(sizeof(struct list_item));

			s->val = strdup($1);
			$$ = (struct list *)malloc(sizeof(struct list));
			SIMPLEQ_INIT($$);
			SIMPLEQ_INSERT_TAIL($$, s, next);
		}
		| str_list COMMA STRING {
			struct list_item	*s = (struct list_item *)malloc(sizeof(struct list_item));

			s->val = strdup($3);
			SIMPLEQ_INSERT_TAIL($1, s, next);
		}
		;

param_list	: VAR STRING {
			struct list_item	*p, *q;

			p = (struct list_item *)malloc(sizeof(struct list_item));
			q = (struct list_item *)malloc(sizeof(struct list_item));
			asprintf((char **)&p->val, "--%s", $1);
			q->val = strdup($2);
			$$ = (struct list *)malloc(sizeof(struct list));
			SIMPLEQ_INIT($$);
			SIMPLEQ_INSERT_TAIL($$, p, next);
			SIMPLEQ_INSERT_TAIL($$, q, next);
		}
		| param_list COMMA VAR STRING {
			struct list_item	*p, *q;

			p = (struct list_item *)malloc(sizeof(struct list_item));
			q = (struct list_item *)malloc(sizeof(struct list_item));
			asprintf((char **)&p->val, "--%s", $3);
			q->val = strdup($4);
			SIMPLEQ_INSERT_TAIL($1, p, next);
			SIMPLEQ_INSERT_TAIL($1, q, next);
		}
		| param_list COMMA VAR LCBRACKET str_list RCBRACKET {
			struct list_item	*p, *q;

			while (!SIMPLEQ_EMPTY($5)) {
				p = (struct list_item *)malloc(sizeof(struct list_item));
				asprintf((char **)&p->val, "--%s", $3);
				SIMPLEQ_INSERT_TAIL($1, p, next);
				q = SIMPLEQ_FIRST($5);
				SIMPLEQ_REMOVE_HEAD($5, next);
				SIMPLEQ_INSERT_TAIL($1, q, next);
			}
		}
		;

application	: APPLICATION STRING {
			conf->c_app_name = strdup($2);
		}
		| APPLICATION STRING LCBRACKET param_list RCBRACKET {
			int			  i = 0;
			int			  argc = 0;
			char			**argv;
			struct list_item	 *p;

			conf->c_app_name = strdup($2);
			SIMPLEQ_FOREACH(p, $4, next)
				++argc;
			++argc;
			argv = (char **)reallocarray(NULL, argc, sizeof(char *));
			argv[0] = strdup($2);
			while (!SIMPLEQ_EMPTY($4)) {
				p = SIMPLEQ_FIRST($4);
				argv[++i] = (char *)p->val;
				SIMPLEQ_REMOVE_HEAD($4, next);
				free(p);
			}
			conf->c_app_argc = argc;
			conf->c_app_argv = argv;
			free($4);
		}
		;

adapt		: ADAPT EVERY NUMBER SEC {
			if ($3 < 1) {
				yyerror("adaptation time must be > 1.");
				YYABORT;
			}
			conf->c_atime = $3;
		}
                | ADAPT EVERY NUMBER MIN {
			if ($3 < 1) {
				yyerror("adaptation time must be > 1.");
				YYABORT;
			}
			conf->c_atime = $3 * 60;
		}
		;

learn		: LEARN USING KMEANSSEQ WITH CLUSTERS EQUAL NUMBER FOR NUMBER SEC {
			if ($9 < 30) {
				yyerror("learning time must be >= 30 sec.");
				YYABORT;
			}
			conf->c_ltime = $9;
			conf->c_lalgo = LEARN_KMEANS_SEQ;
			conf->c_lparam = $7;
		}
		| LEARN USING KMEANSSEQ WITH CLUSTERS EQUAL NUMBER FOR NUMBER MIN {
			if ($9 < 1) {
				yyerror("use you may use seconds for learning time.");
				YYABORT;
			}
			conf->c_ltime = $9 * 60;
			conf->c_lalgo = LEARN_KMEANS_SEQ;
			conf->c_lparam = $7;
		}
		| LEARN USING KMEANSINCR WITH THRESHOLD EQUAL NUMBER FOR NUMBER SEC {
			if ($9 < 30) {
				yyerror("learning time must be >= 30 sec.");
				YYABORT;
			}
			conf->c_ltime = $9;
			conf->c_lalgo = LEARN_KMEANS_INCR;
			conf->c_lparam = $7;
		}
		| LEARN USING KMEANSINCR WITH THRESHOLD EQUAL NUMBER FOR NUMBER MIN {
			if ($9 < 1) {
				yyerror("use you may use seconds for learning time.");
				YYABORT;
			}
			conf->c_ltime = $9 * 60;
			conf->c_lalgo = LEARN_KMEANS_INCR;
			conf->c_lparam = $7;
		}
		;

log		: LOG TO STRING {
			conf->c_logfile = strdup($3);
		}
		;

host_list	: HOST STRING AT DPID STRING PORT NUMBER {
			struct host_info	*h = (struct host_info *)malloc(sizeof(struct host_info));
			struct list_item	*p = (struct list_item *)malloc(sizeof(struct list_item));

			if (!inet_aton($2, &h->ip_addr))
				errx(1, "invalid IP address");
			if (!(h->dpid = dpid_aton($5)))
				errx(1, "invalid DPID");
			h->port = $7;

			p->val = h;
			$$ = (struct list *)malloc(sizeof(struct list));
			SIMPLEQ_INIT($$);
			SIMPLEQ_INSERT_TAIL($$, p, next);
		}
		| host_list COMMA HOST STRING AT DPID STRING PORT NUMBER {
			struct host_info        *h = (struct host_info *)malloc(sizeof(struct host_info));
			struct list_item	*p = (struct list_item *)malloc(sizeof(struct list_item));

			if (!inet_aton($4, &h->ip_addr))
				errx(1, "invalid IP address");
			if (!(h->dpid = dpid_aton($7)))
				errx(1, "invalid DPID");
			h->port = $9;

			p->val = h;
			SIMPLEQ_INSERT_TAIL($1, p, next);
		}
		;

topology	: TOPOLOGY LCBRACKET host_list RCBRACKET {
			int			  i = 0, n = 0;
			struct host_info	**hosts;
			struct list_item	 *p;

			SIMPLEQ_FOREACH(p, $3, next)
				++n;
			hosts = (struct host_info **)reallocarray(NULL, n, sizeof(struct host_info *));
			while (!SIMPLEQ_EMPTY($3)) {
				p = SIMPLEQ_FIRST($3);
				hosts[i++] = (struct host_info *)p->val;
				SIMPLEQ_REMOVE_HEAD($3, next);
				free(p);
			}
			conf->c_topo_n = n;
			conf->c_topo = hosts;
			free($3);
		}
		;
%%

static ofp_fptr
check_ofp_version(const char *ver)
{
	int	i;

	for (i = 0 ; i < N_OFP_VERS ; i++)
		if (!strcmp(ver, ofp_vers[i].ver))
			return ofp_vers[i].func;
	return NULL;
}

int
yyerror(const char *fmt, ...)
{
	va_list	ap;

	va_start(ap, fmt);
	fprintf(stdout, "%s:%d: ", filename, yylineno);
	vfprintf(stdout, fmt, ap);
	fprintf(stdout, "\n");
	va_end(ap);

	return 0;
}

int
parse_config(struct config *c, const char *file)
{
	int	ret;

	if (c == NULL)
		errx(1, "parse_config");
	if ((yyin = fopen(file, "r")) == NULL) {
		errx(1, "failed to load config file");
	}
	filename = strdup(file);
	conf = c;
	conf->c_app_name = NULL;
	conf->c_logfile = NULL;
	ret = yyparse();
	fclose(yyin);

	return ret;
}
