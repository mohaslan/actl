/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <libof.h>
#include <of10.h>

#include <net/if_arp.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>

#include <actl.h>

#include <stdio.h>
#include <stdlib.h>

static struct actl_ctx		*ctx;

int
init(struct actl_ctx *c)
{
	ctx = c;
	printf("l2_hub initialized.\n");
	return 0;
}

int
main(int argc, char **argv)
{
	printf("L2_hub ready.\n");
	return 0;
}

void
handler(struct of_event *ev)
{
	struct ether_header		*eh;
	struct of10_packet_in		*ph;
	struct of10_packet_out		*msg_out;
	struct of10_action_output 	*action;

	if (ev->type != OFEV_PROTO_MESSAGE)
		return;

	ph = (struct of10_packet_in *)(ev->ofp_hdr);
	eh = (struct ether_header *)(ph->data);

#ifdef DEBUG
	printf("in_port: %hu, ", ntohs(ph->in_port));
	printf("%x:%x:%x:%x:%x:%x ->", eh->ether_shost[0], eh->ether_shost[1],
		eh->ether_shost[2], eh->ether_shost[3], eh->ether_shost[4],
		eh->ether_shost[5]);
	printf(" %x:%x:%x:%x:%x:%x, ", eh->ether_dhost[0], eh->ether_dhost[1],
		eh->ether_dhost[2], eh->ether_dhost[3], eh->ether_dhost[4],
		eh->ether_dhost[5]);
	printf("type: 0x%hx ", ntohs(eh->ether_type));
	if (ntohs(eh->ether_type) == ETHERTYPE_ARP)
		printf("[ARP]");
	else if (ntohs(eh->ether_type) == ETHERTYPE_IP)
		printf("[IP4]");
	printf("\n");
#endif

	/* flood */
	msg_out = (struct of10_packet_out *)malloc(sizeof(struct of10_packet_out) + sizeof(struct of10_action_output));
	if (msg_out == NULL)
		return;
	msg_out->hdr.version = OFP_VERSION_10;
	msg_out->hdr.type = OFPT10_PACKET_OUT;
	msg_out->hdr.length = htons(sizeof(struct of10_packet_out) + sizeof(struct of10_action_output));
	msg_out->hdr.xid = 0;
	msg_out->buffer_id = ph->buffer_id;
	msg_out->in_port = ph->in_port;
	msg_out->actions_len = htons(sizeof(struct of10_action_output));
	action = (struct of10_action_output *)(msg_out->actions);
	action->type = htons(OFPAT10_OUTPUT);
	action->len = htons(sizeof(struct of10_action_output));
	action->port = htons(OFPP10_FLOOD);
	action->max_len = 0;
	ctx->cntl->send(ctx->cntl, ev->dp, (struct ofp_header *)msg_out);
	free(msg_out);
}

double
kpi()
{
	return 0;
}
