/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef ACTL_H
#define ACTL_H

#include <libof.h>
#include <dht.h>
#include <netinet/in.h>
#include "clustering.h"


struct host_info {
	struct in_addr		ip_addr;
	uint64_t		dpid;
	uint16_t		port;
};

struct actl_ctx {
	struct config		*conf;
	struct dht_node		*node;
	struct of_controller	*cntl;
	int			 adaptive;
	int			 learning;
	int			 learn_perval;
	double			 target_kpi;
	double			 learning_phi;
	union {
		struct kmeans_seq	kseq;
		struct kmeans_incr	kincr;
	}; /* C11 */
	void			(*adapt)(struct actl_ctx *, double);
	int			(*put)(struct actl_ctx *, const char *, const char *);
	int			(*get)(struct actl_ctx *, const char *, char **);
	void			(*topo)(struct actl_ctx *, struct host_info ***, int *);
};

struct app{
	char	*name;
	int	(*a_init)(struct actl_ctx *);
	int	(*a_main)(int, char **);
	void	(*a_handler)(struct of_event *);
	double	(*a_kpi)();
};

#endif
