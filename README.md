Adaptive SDN Controllers Research Project
=========================================

About
-----
> Are ones that can autonomously and dynamically
tune their configuration in order to achieve a
certain level of performance measured in predefined
metrics and based on the application requirements.

Why?
----
* Reduce the complexity at the applications
* React rapidly to the changing network conditions
* Reduce the overhead of controllers state distribution

People
------
Adaptive SDN Controllers is a research project of [Prof. Ahraf Matrawy's Research Group](http://www.csit.carleton.ca/~amatrawy/sdn.html).

* [Mohamed Aslan](http://www.sce.carleton.ca/~maslan/) - Lead developer
* [Ashraf Matrawy, PhD](http://www.csit.carleton.ca/~amatrawy/) - Supervisor

Publications
------------
* **Mohamed Aslan**, and Ashraf Matrawy,
"[Adaptive Consistency for Distributed SDN Controllers,](http://networks2016.etsmtl.ca/)"
in the Proc. of the 17th International Network Strategy and Planning Symposium (Networks 2016), Montreal, QC, Canada. September 2016.
\[[PDF](http://www.sce.carleton.ca/~maslan/files/sdn-adaptive.pdf) | [BibTex](https://scholar.googleusercontent.com/citations?view_op%3Dexport_citations%26user%3DLbnNTvsAAAAJ%26s%3DLbnNTvsAAAAJ%3A_FxGoFyzp5QC%26citsig%3DAMstHGQAAAAAWDOa6yIXchx9wIBBrr7xV8tdzMx0l5xh%26hl%3Den%26cit_fmt%3D0)\]

* **Mohamed Aslan**, and Ashraf Matrawy,
"[On the Impact of Network State Collection on the Performance of SDN Applications,](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=7314891)" IEEE Communications Letters, vol. 20, no. 1, pp. 5-8, January 2016.
\[[PDF](http://www.sce.carleton.ca/~maslan/files/sdn-perf.pdf) | [BibTex](https://scholar.googleusercontent.com/citations?view_op%3Dexport_citations%26user%3DLbnNTvsAAAAJ%26s%3DLbnNTvsAAAAJ%3AWF5omc3nYNoC%26citsig%3DAMstHGQAAAAAWDOa0P3M1fyOznZvE7S_8o0RY8eP_kRA%26hl%3Den%26cit_fmt%3D0)\]

Presentations and Talks
-----------------------
* [Sep. 27, 2016](http://www.sce.carleton.ca/~maslan/files/slides-net2016-adaptive.pdf)
"Adaptive Consistency for Distributed SDN Controllers" @ [International Network Strategy and Planning Symposium](http://networks2016.etsmtl.ca/)

Development
-----------
Our main code repository can be accessed at:

* `git clone https://bitbucket.org/mohaslan/actl.git`

Additional necessary components can be accessed at:

* `git clone https://bitbucket.org/mohaslan/libof.git`
* `git clone https://bitbucket.org/mohaslan/libdht.git`
* `git clone https://bitbucket.org/mohaslan/hashtab.git`

You can also view our repositories **online** at:

* [actl](https://bitbucket.org/mohaslan/actl)
* [libof](https://bitbucket.org/mohaslan/libof)
* [libdht](https://bitbucket.org/mohaslan/libdht)
* [hashtab](https://bitbucket.org/mohaslan/hashtab)

* * *

Copyright &copy; 2015 - 2016 [Mohamed Aslan](http://www.sce.carleton.ca/~maslan/)
