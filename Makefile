PROG=		actl
BINDIR=		/usr/bin
SRCS=		actl.c parse.y lex.l utils.c
SRCS+=		clustering.c model.c
MAN=		actl.1

CFLAGS+=	-DDEBUG -g
#CFLAGS+=	-g
COPTS+=		-Wall
LDADD+=		-ldht -lhashtab -lpthread -lof

SUBDIR=		app-l2_hub app-l2_learn app-l3_lb

.include <bsd.prog.mk>
.include <bsd.subdir.mk>
