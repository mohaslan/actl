LEX=lex
YACC= yacc
CFLAGS+= -Wall -I. -g $(shell pkg-config --cflags libkqueue)
CFLAGS+= -include bsd/stdlib.h
LDLIBS+= -ldl -lof -ldht -lpthread -lhashtab $(shell pkg-config --libs libkqueue libbsd)
SRCS=$(wildcard *.c)
OBJS=$(patsubst %.c, %.o, $(SRCS))
all: actl apps
%.o: %.c 
	$(CC) -o $@ -c $< $(CFLAGS)
y.tab.c y.tab.h: parse.y
	$(YACC) -d parse.y
lex.yy.c: lex.l
	$(LEX) lex.l
actl: $(OBJS) y.tab.o lex.yy.o
	$(CC) -o $@ $^ $(CFLAGS) $(LDLIBS)
apps:
	$(MAKE) -C app-l2_hub -f Makefile.gnu
	$(MAKE) -C app-l2_learn -f Makefile.gnu
	$(MAKE) -C app-l3_lb -f Makefile.gnu
apps_install: apps
	$(MAKE) -C app-l2_hub -f Makefile.gnu install
	$(MAKE) -C app-l2_learn -f Makefile.gnu install
	$(MAKE) -C app-l3_lb -f Makefile.gnu install
install: actl apps_install
	install -m 0555 actl /usr/sbin/actl
apps_clean:
	$(MAKE) -C app-l2_hub -f Makefile.gnu clean
	$(MAKE) -C app-l2_learn -f Makefile.gnu clean
	$(MAKE) -C app-l3_lb -f Makefile.gnu clean
clean: apps_clean
	rm -f *.o y.tab.* lex.yy.*
