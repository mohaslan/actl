/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

%{
#include <stdio.h>
#include "y.tab.h"

extern int yyerror(const char *fmt, ...);
%}

%option yylineno
%option noyywrap
%option noinput
%option nounput

%x dquotes

%%

id				{ return ID; }
listen				{ return LISTEN; }
on				{ return ON; }
peers				{ return PEERS; }
controller			{ return CONTROLLER; }
ip				{ return IP; }
port				{ return PORT; }
replicas			{ return REPLICAS; }
openflow			{ return OPENFLOW; }
version				{ return VERSION; }
application			{ return APPLICATION; }
adapt				{ return ADAPT; }
every				{ return EVERY; }
learn				{ return LEARN; }
using				{ return USING; }
kmeans_seq			{ return KMEANSSEQ; }
kmeans_incr			{ return KMEANSINCR; }
with				{ return WITH; }
clusters			{ return CLUSTERS; }
threshold			{ return THRESHOLD; }
for				{ return FOR; }
sec				{ return SEC; }
min				{ return MIN; }
log				{ return LOG; }
to				{ return TO; }
topology			{ return TOPOLOGY; }
host				{ return HOST; }
at				{ return AT; }
dpid				{ return DPID; }

yes				{ yylval.i = 1; return BOOL; }
no				{ yylval.i = 0; return BOOL; }

,				{ return COMMA; }

\{				{ return LCBRACKET;}
\}				{ return RCBRACKET;}

=				{ return EQUAL; }

[0-9]+				{ yylval.i = atoi(yytext); return NUMBER; }
0x[0-9a-fA-F]+			{ return NUMBER; }

\"				{ BEGIN(dquotes); }
<dquotes>[^\"]*\"		{
					yylval.s = strndup(yytext, yyleng - 1);
					BEGIN(INITIAL);
					return STRING;
				}

[\_a-zA-Z][\_a-zA-Z0-9]*	{ yylval.s = strdup(yytext); return VAR; }

[ \t\n]				{ ; }
\#.*\n				{ ; }
.				{ yyerror("error: invalid character: %s", yytext); }

%%
