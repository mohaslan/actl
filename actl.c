/*
 * Copyright (c) 2016 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <err.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dlfcn.h>
#include <arpa/inet.h>

#include <hashtab.h>
#include <dht.h>
#include <libof.h>
#include <of10.h>

#include "actl.h"
#include "conf.h"
#include "utils.h"
#include "model.h"
#include "clustering.h"


static struct actl_ctx		ctx;


static void
adapt(struct actl_ctx *c, double kpi)
{
	c->target_kpi = kpi;
	c->adaptive = 1;
}

static double
map(struct actl_ctx *c, double kpi)
{
	if (c->conf->c_lalgo == LEARN_KMEANS_SEQ)
		return kmeans_seq_find(&c->kseq, kpi);
	else
		return kmeans_incr_find(&c->kincr, kpi);
}

static int
put(struct actl_ctx *c, const char *key, const char *val)
{
	double			target_phi;
	int			r = 1, w = 1;
	struct timespec		ts;

	if (c->adaptive) {
		target_phi = c->learning ? c->learning_phi : map(c, c->target_kpi);
		phi2rw(target_phi, c->conf->c_nreplicas, &r, &w);
		printf("[INFO] target: KPI = %lf -> Phi = %lf -> (R = %d, W = %d).\n", ctx.target_kpi, target_phi, r, w);
	}
	(void)clock_gettime(CLOCK_REALTIME, &ts);
	return dht_put_tunable(c->node, key, val, &ts, w);
}

static int
get(struct actl_ctx *c, const char *key, char **val)
{
	double			target_phi;
	int			r = 1, w = 1;

	if (c->adaptive) {
		target_phi = c->learning ? c->learning_phi : map(c, c->target_kpi);
		phi2rw(target_phi, c->conf->c_nreplicas, &r, &w);
		printf("[INFO] target: KPI = %lf -> Phi = %lf -> (R = %d, W = %d).\n", ctx.target_kpi, target_phi, r, w);
	}
	return dht_get_tunable(c->node, key, val, r);
}

static void
topo(struct actl_ctx *c, struct host_info ***hosts, int *n)
{
	*hosts = c->conf->c_topo;
	*n = c->conf->c_topo_n;
}

static void
init_context(struct actl_ctx *c)
{
	if (!c)
		return;
	c->cntl = (struct of_controller *)xmalloc(sizeof(struct of_controller));
	c->node = (struct dht_node *)xmalloc(sizeof(struct dht_node));
	c->conf = (struct config *)xmalloc(sizeof(struct config));
	c->adaptive = 0;
	c->learning = 1;
	c->adapt = adapt;
	c->learning_phi = 0;
	c->put = put;
	c->get = get;
	c->topo = topo;
}

static void
load_app(struct actl_ctx *c)
{
	if (!c)
		return;
	/* load application */
	c->conf->c_app_so = dlopen(c->conf->c_app_name, RTLD_NOW);
	if (!c->conf->c_app_so)
		errx(1, "error loading application %s: %s", c->conf->c_app_name, dlerror());
	/* init */
	c->conf->c_app.a_init = dlsym(c->conf->c_app_so, "init");
	if (!(c->conf->c_app.a_init))
		errx(1, "error loading symbol from %s: %s", c->conf->c_app_name, dlerror());
	/* main */
	c->conf->c_app.a_main = dlsym(c->conf->c_app_so, "main");
	if (!c->conf->c_app.a_main)
		errx(1, "error loading symbol from %s: %s", c->conf->c_app_name, dlerror());
	/* handler */
	c->conf->c_app.a_handler = dlsym(c->conf->c_app_so, "handler");
	if (!c->conf->c_app.a_handler)
		errx(1, "error loading symbol from %s: %s", c->conf->c_app_name, dlerror());
	/* handler */
	c->conf->c_app.a_kpi = dlsym(c->conf->c_app_so, "kpi");
	if (!c->conf->c_app.a_kpi)
		errx(1, "error loading symbol from %s: %s", c->conf->c_app_name, dlerror());
}

static void
poll_kpi(struct of_controller *ctl)
{
	double          cur_kpi, cur_phi, tar_phi;

	if (ctx.conf->c_app_so && ctx.adaptive) {
		cur_kpi = ctx.conf->c_app.a_kpi();
		if (ctx.learning) {
			if (ctx.conf->c_lalgo == LEARN_KMEANS_SEQ)
				kmeans_seq_insert(&ctx.kseq, cur_kpi, ctx.learning_phi);
			else
				kmeans_incr_insert(&ctx.kincr, cur_kpi, ctx.learning_phi);
			printf("[INFO] learned KPI = %lf -> Phi = %lf.\n", cur_kpi, ctx.learning_phi);
		} else {
			/* TODO: insert point */
			cur_phi = map(&ctx, cur_kpi);
			tar_phi = map(&ctx, ctx.target_kpi);
			printf("[INFO] KPI = (target %lf, current %lf), Phi = (target %lf, current %lf).\n", ctx.target_kpi, cur_kpi, tar_phi, cur_phi);
			f_log(ctx.conf->c_logfp, "%lf, %lf, %lf, %lf\n", ctx.target_kpi, tar_phi, cur_kpi, cur_phi);
		}
	}
}

static void
learn_timer(struct of_controller *ctl)
{
	if (ctx.conf->c_app_so && ctx.adaptive && ctx.learning) {
		ctx.conf->c_ltime -= ctx.learn_perval;
		if (ctx.conf->c_ltime <= 0) {
			ctx.learning = 0;
			printf("[INFO] learning finished.\n");
			if (ctx.conf->c_lalgo == LEARN_KMEANS_SEQ)
				kmeans_seq_print(&ctx.kseq, "LEARN");
			else
				kmeans_incr_print(&ctx.kincr, "LEARN");
		}
		ctx.learning_phi += 0.1;
	}
}

static void *
dht_thread(void *arg)
{
	dht_event_loop(ctx.node);
	return NULL;
}

static void *
controller_thread(void *arg)
{
	/* init default app */
	if (ctx.conf->c_app_so) {
		optind = 0; /* make sure apps can use getopt(3) or getopt_long(3) */
		ctx.conf->c_app.a_init(&ctx);
		printf("[INFO] adaptation period is %d sec.\n", ctx.conf->c_atime);
		ctx.conf->c_app.a_main(ctx.conf->c_app_argc, ctx.conf->c_app_argv);
	}
	ctx.learn_perval = ctx.conf->c_ltime / 10;
	ctx.cntl->timer(ctx.cntl, ctx.learn_perval * 1000, learn_timer);
	/* poll application-specific KPI */
	ctx.cntl->timer(ctx.cntl, ctx.conf->c_atime * 1000, poll_kpi);
	ctx.cntl->loop(ctx.cntl);
	return NULL;
}

static void
handler(struct of_controller *ctl, struct of_event *ev)
{
#ifdef DEBUG
	printf("actl: handler.\n");
#endif
	if (ctx.conf->c_app_so)
		ctx.conf->c_app.a_handler(ev);
}

int
main(int argc, char **argv)
{
	char			*homedir, *conf_file;
	int			 ch, i, flag_altconf = 0, flag_error = 0;
	pthread_t		 dtid, ctid;


	/* init global context */
	init_context(&ctx);

	/* parse command line options */
	while ((ch = getopt(argc, argv, "c:")) != -1) {
		switch (ch) {
		case 'c':
			flag_altconf = 1;
			conf_file = strdup(optarg);
			break;
		default:
			flag_error = 1;
			break;
		}
	}
	argc -= optind;
	argv += optind;

	if (flag_error) {
		return EXIT_FAILURE; /* TODO: usage */
	}
	if (!flag_altconf) {
		if ((homedir = getenv("HOME")) == NULL) {
			/* TODO: look in current directory */
			errx(1, "failed to read environmental variable \'$HOME\'");
		}
		asprintf(&conf_file, "%s/%s", homedir, CONFFILENAME);	/* XXX: null */
	}
	/* parse config file */
	if (parse_config(ctx.conf, conf_file))
		errx(1, "error parsing config file.");

	/* init clustering algorithms */
	if (ctx.conf->c_lalgo == LEARN_KMEANS_SEQ)
		kmeans_seq_init(&ctx.kseq, ctx.conf->c_lparam);
	else
		kmeans_incr_init(&ctx.kincr, ((ctx.conf->c_lparam * 1.0) / 100));

	/* open log file */
	if (ctx.conf->c_logfile) {
		ctx.conf->c_logfp = fopen(ctx.conf->c_logfile, "w");
		if (!ctx.conf->c_logfp)
			errx(1, "error creating log file.");
	}

	/* load application */
	load_app(&ctx);

#ifdef DEBUG
	printf("config info...\n");
	printf("\tnode id \'%s\', control port \'%d\', switch port \'%d\'.\n",
		ctx.conf->c_nodeid, ctx.conf->c_co_port, ctx.conf->c_sw_port);
	printf("\topenflow version \'%s\'.\n", ctx.conf->c_ofp_verstr);
	printf("\tnumber of replicas \'%d\'.\n", ctx.conf->c_nreplicas);
#endif

	/* initialize DHT */
	if (!dht_init(ctx.node, ctx.conf->c_nodeid, ctx.conf->c_co_port, ctx.conf->c_nreplicas, 0, NULL))
		errx(1, "dht_init");
	else
		printf("dht init successful.\n");
	for (i = 0 ; i < ctx.conf->c_npeers ; i++) {
		dht_add_peer(ctx.node, ctx.conf->c_peers[i].node_id, inet_ntoa(ctx.conf->c_peers[i].ip_addr), ctx.conf->c_peers[i].port);
		printf("\tpeer %d: id: \'%s\', ip=%s, port=%u.\n", (i + 1), ctx.conf->c_peers[i].node_id, inet_ntoa(ctx.conf->c_peers[i].ip_addr), ctx.conf->c_peers[i].port);
	}
	pthread_create(&dtid, NULL, dht_thread, NULL);

	/* initialize OF controller */
	if (of_controller_init(ctx.cntl, ctx.conf->c_sw_port, ctx.conf->c_ofp()))
		errx(1, "init failed.");
	else
		printf("controller init successful.\n");
	ctx.cntl->handler(ctx.cntl, handler);
	pthread_create(&ctid, NULL, controller_thread, NULL);

	/* wait */
	pthread_join(dtid, NULL);
	pthread_join(ctid, NULL);

	fclose(ctx.conf->c_logfp);
	return 0;
}
