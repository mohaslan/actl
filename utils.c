/*
 * Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <err.h>
#include "utils.h"

void *
xmalloc(size_t size)
{
	void *ptr;

	if ((ptr = malloc(size)) == NULL)
		err(1, "malloc %zu", size);
	return ptr;
}

void *
xcalloc(size_t nmemb, size_t size)
{
	void *ptr;

	if ((ptr = calloc(nmemb, size)) == NULL)
		err(1, "calloc %zu", size);
	return ptr;
}

void
f_log(FILE *lg, const char *fmt, ...)
{
	va_list			ap;

	if (!lg)
		return;
	va_start(ap, fmt);
	if (vfprintf(lg, fmt, ap) < -1)
		errx(1, "vfprintf");
	va_end(ap);
	fflush(lg);
}

uint64_t
dpid_aton(const char *str)
{
	int			i, ret, digits[8];
	uint64_t		dpid = 0;

	ret = sscanf(str, "%2x:%2x:%2x:%2x:%2x:%2x:%2x:%2x", &digits[0],
		&digits[1], &digits[2], &digits[3], &digits[4], &digits[5],
		&digits[6], &digits[7]);
	if (ret != 8)
		return 0;
	for (i = 0 ; i < 8 ; i++) {
		dpid <<= 8;
                dpid |= digits[i];
	}
	return dpid;
}
