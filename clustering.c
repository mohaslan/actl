/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stddef.h>
#include "clustering.h"

static int
nearest_head(double x, struct cluster *clusters, int n_clusters)
{
	int	i;
	int	min_idx = 0;
	double	min_dist = fabs(x - clusters[0].head);	/* FIXME */

	if (n_clusters == 1)
		return 0;

	for (i = 1 ; i < n_clusters ; i++) {
		if (fabs(x - clusters[i].head) < min_dist) {
			min_dist = fabs(x - clusters[i].head);
			min_idx = i;
		}
	}

	return min_idx;
}


void
kmeans_seq_init(struct kmeans_seq *kseq, size_t n)
{
	int	i;

	kseq->n_points = 0;
	kseq->n_clusters = n;
	kseq->clusters = (struct cluster *)reallocarray(NULL, n, sizeof(struct cluster));
	for (i = 0 ; i < n ; i++) {
		kseq->clusters[i].head = 0;
		kseq->clusters[i].val = 0;
		kseq->clusters[i].n_points = 0;
	}
}

void
kmeans_seq_insert(struct kmeans_seq *kseq, double k, double v)
{
	int	near;

	if (kseq->n_points < kseq->n_clusters) {
		kseq->clusters[kseq->n_points].head = k;
		kseq->clusters[kseq->n_points].val = v;
		kseq->clusters[kseq->n_points++].n_points++;
	} else {
		near = nearest_head(k, kseq->clusters, kseq->n_clusters);
		/* XXX: lock */
		kseq->clusters[near].head =
			(kseq->clusters[near].head * kseq->clusters[near].n_points + k) / (kseq->clusters[near].n_points + 1);
		kseq->clusters[near].val =
			(kseq->clusters[near].val * kseq->clusters[near].n_points + v) / (kseq->clusters[near].n_points + 1);
		kseq->clusters[near].n_points++;
	}
}

double
kmeans_seq_find(struct kmeans_seq *kseq, double k)
{
	int	near;

	near = nearest_head(k, kseq->clusters, kseq->n_clusters);
	return kseq->clusters[near].val;
}

void
kmeans_seq_print(struct kmeans_seq *kseq, const char *prefix)
{
	int	i;

	printf("[%s] kmeans_seq n_clusters: %zu\n", prefix, kseq->n_clusters);
	for (i = 0 ; i < kseq->n_clusters ; i++)
		printf("[%s] C(%d): (%lf, %lf, %zu)\n", prefix, i, kseq->clusters[i].head, kseq->clusters[i].val, kseq->clusters[i].n_points);
}

void
kmeans_incr_init(struct kmeans_incr *kincr, double thresh)
{
	int	i;

	kincr->threshold = thresh;
	kincr->n_clusters = 0;
	kincr->clusters = (struct cluster *)reallocarray(NULL, KMEANS_INCR_MAXTHRESH, sizeof(struct cluster));
	for (i = 0 ; i < KMEANS_INCR_MAXTHRESH ; i++) {
		kincr->clusters[i].head = 0;
		kincr->clusters[i].val = 0;
		kincr->clusters[i].n_points = 0;
	}
}

static void
new_cluster(struct kmeans_incr *kincr, double k, double v)
{
	kincr->clusters[kincr->n_clusters].head = k;
	kincr->clusters[kincr->n_clusters].val = v;
	kincr->clusters[kincr->n_clusters].n_points = 1;
	kincr->n_clusters++;
}

void
kmeans_incr_insert(struct kmeans_incr *kincr, double k, double v)
{
	int	near;
	double	dist;

	/* first point? */
	if (!(kincr->n_clusters)) {
		new_cluster(kincr, k, v);
		return;
	}
	near = nearest_head(k, kincr->clusters, kincr->n_clusters);
	/* head is zero? */
	if (!(kincr->clusters[near].head)) {
		new_cluster(kincr, k, v);
		return;
	}
	dist = fabs(kincr->clusters[near].head - k) / kincr->clusters[near].head;
	/* dist < threshold or max clusters is reached? */
	if ((dist < kincr->threshold) || (kincr->n_clusters == KMEANS_INCR_MAXTHRESH)) {
		/* XXX: lock */
		kincr->clusters[near].head =
			(kincr->clusters[near].head * kincr->clusters[near].n_points + k) / (kincr->clusters[near].n_points + 1);
		kincr->clusters[near].val =
			(kincr->clusters[near].val * kincr->clusters[near].n_points + v) / (kincr->clusters[near].n_points + 1);
		kincr->clusters[near].n_points++;
		return;
	}
	new_cluster(kincr, k, v);
}

double
kmeans_incr_find(struct kmeans_incr *kincr, double k)
{
	int	near;

	near = nearest_head(k, kincr->clusters, kincr->n_clusters);
	return kincr->clusters[near].val;
}

void
kmeans_incr_print(struct kmeans_incr *kincr, const char *prefix)
{
	int	i;

	printf("[%s] kmeans_seq n_clusters: %zu\n", prefix, kincr->n_clusters);
	printf("[%s] kmeans_incr threshold: %lf\n", prefix, kincr->threshold);
	for (i = 0 ; i < kincr->n_clusters ; i++)
		printf("[%s] C(%d): (%lf, %lf, %zu)\n", prefix, i, kincr->clusters[i].head, kincr->clusters[i].val, kincr->clusters[i].n_points);
}
